const gulp = require("gulp");
const sass = require("gulp-sass");
const del = require("del");
const autoprefixer = require("autoprefixer");
const sourcemaps = require("gulp-sourcemaps");
const rename = require("gulp-rename");
const concat = require("gulp-concat");
const postcss = require("gulp-postcss");
const gulpHandlebars = require("gulp-compile-handlebars");
const handlebars = require("handlebars");
const fs = require("fs");
const path = require("path");
const exec = require("child_process").exec;

const HBS_ROOT_PATH = "src/pages/";
const HBS_DATA_PATH = "src/config/";

gulp.task("default", ["beforeBuild"]);

gulp.task("beforeBuild", ["clear"], () => {
  gulp.start("build");
});

gulp.task("build", ["build:pages", "build:js", "build:css", "copy:statics", "precompile-templates"]);

gulp.task("copy:statics", ["copy:images", "copy:fonts", "copy:data"]);

gulp.task("copy:images", () => {
  return gulp.src("src/static/img/**/*")
    .pipe(gulp.dest("dist/resources/img"));
});

gulp.task("copy:data", () => {
  return gulp.src("src/static/data/**/*")
    .pipe(gulp.dest("dist/resources/data"));
});

gulp.task("copy:fonts", () => {
  return gulp.src("src/static/fonts/**/*")
    .pipe(gulp.dest("dist/resources/fonts"));
});

gulp.task("clear", () => {
  return del('dist/**/*');
});

gulp.task("build:pages", () => {
  let data = {};
  let options = {
    helpers: {
      include: includePartial
    },
    batch: ["src/pages/modules/",
            "src/pages/templates/"]
  };
  return gulp.src("src/pages/*.hbs")
    .pipe(gulpHandlebars(data, options))
    .pipe(rename({
      extname: ".html"
    }))
    .pipe(gulp.dest("dist"));
});

gulp.task("build:css", () => {
  return gulp.src("src/scss/*.scss")
    .pipe(sourcemaps.init())
    .pipe(sass())
    .pipe(postcss([autoprefixer]))
    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest("dist/resources/css"));
});

gulp.task("build:js", ["js:main", "js:libs"])

gulp.task("js:main", (cb) => {
  exec(`webpack`, (err, stdout, stderr) => {
    if (stderr) {
      console.log(stderr);
    }
    console.log(stdout);
    cb(err);
  });
});

gulp.task("js:libs", (cb) => {
  return gulp.src("src/static/js/libs/*.js")
    .pipe(concat("libs.js"))
    .pipe(gulp.dest("dist/resources/js/"));
});

gulp.task("precompile-templates", (cb) => {
  let handlebarsTemplatesPath = path.resolve(process.cwd(), "./src/pages/templates/").replace(/\\/g, "/");
  exec(`handlebars ${handlebarsTemplatesPath} -f ./dist/resources/js/precompiled-templates.js -m -e hbs`, (err, stdout, stderr) => {
    if (stderr) {
      console.log(stderr);
    }
    console.log(stdout);
    cb(err);
  });
});

/* Handlebars custom helper */

function includePartial(tplName, jsonName) {
  let template = fs.readFileSync(`${HBS_ROOT_PATH}/${tplName}.hbs`, "utf-8");
  let data = (typeof jsonName === "string") ? JSON.parse(fs.readFileSync(`${HBS_DATA_PATH}/${jsonName}.json`, "utf-8")) : {};
  let compiledTemplate = handlebars.compile(template);
  return compiledTemplate(data);
}