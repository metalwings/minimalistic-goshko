declare let mediumZoom: any;

import { Bubble } from "./components/Bubble";
import { OverlayCloseButton } from "./components/overlay/OverlayCloseButton";
import { Overlay } from "./components/overlay/Overlay";
import { EventDispatcher } from "./common/EventDispatcher";

document.addEventListener("DOMContentLoaded", () => {
  let components = {
    eventsDispatcher: new EventDispatcher(),
    bubble: new Bubble(),
    overlay: new Overlay(),
    overlayCloseButton: new OverlayCloseButton()
  }

  mediumZoom(".post__image--zoomable img", {
    background: "rgba(29, 29, 29, 0.89)"
  });
});