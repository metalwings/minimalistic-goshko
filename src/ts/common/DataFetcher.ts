import { URLUtils } from "../utils/URLUtils";

interface IPostFetchRequest {
  limit?: number;
  offset?: number;
  view?: string;
  slug?: string;
}

export class DataFetcher {

  private static API_ENDPOINT = "https://data.goshko.de/api/2.0";
  private static headers = {
    "Authorization": "Basic cHVibGljYXBpOg==",
    "Access-Control-Allow-Origin": "*",
    "Content-Type": "application/json; charset=utf-8"
  };

  public static async fetchPosts(config: IPostFetchRequest = {}) {
    let limit = config.limit || 2,
        offset = config.offset || 0,
        view = config.view || "",
        parameters = `?${URLUtils.jsonAsGETParameters({ limit, offset })}`,
        url = new URL(`${this.API_ENDPOINT}/posts/${view}${parameters}`, window.location.origin).href;

    return await this.fetchURL(url);
  }

  public static async fetchPostBySlug(config: IPostFetchRequest = {}) {
    if (!config.slug) {
      return null;
    }

    let view = config.view || "",
        slug = config.slug || null,
        parameters = `?filters[slug][like]=${slug}`;

    let url = new URL(`${this.API_ENDPOINT}/posts/${view}${parameters}`, window.location.origin).href;
    return await this.fetchURL(url);
  }

  public static async fetchURL(url: string) {
    return await fetch(url, {
      method: "GET",
      headers: this.headers,
    }).then((response) => response.json());
  }

}