export class EventDispatcher {

  public static triggerEvent(eventName: string = "", detail: any = {}) {
    let eventToTrigger = new CustomEvent(eventName, {detail});
    document.dispatchEvent(eventToTrigger);
  }

}