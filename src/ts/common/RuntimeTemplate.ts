export class RuntimeTemplate {

  static compile(template: string, context: any): string {
    return Handlebars.templates[template](context);
  }

}