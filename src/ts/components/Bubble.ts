import { EventDispatcher } from "../common/EventDispatcher";

export class Bubble {

  private element: HTMLElement;
  private radius: number;

  constructor() {
    this.element = document.querySelector(".bubble");
    this.radius = this.element.offsetWidth / 2;
    this.setEvents();
  }

  setEvents() {
    document.addEventListener("growBubble", this.onGrowBubble.bind(this));
    document.addEventListener("shrinkBubble", this.onShrinkBubble.bind(this));
  }

  onGrowBubble(event: CustomEvent) {
    let data: any = event.detail;
    this.moveTo(data.x, data.y);
    this.grow();
    this.showOverlay();
  }

  onShrinkBubble() {
    this.shrink();
  }

  moveTo(x: number, y: number) {
    this.element.style.top = `${y - this.radius}px`;
    this.element.style.left = `${x - this.radius}px`;
  }

  grow() {
    this.element.classList.add("grow");
  }

  shrink() {
    this.element.classList.remove("grow");
  }

  showOverlay() {
    setTimeout(() => {
      EventDispatcher.triggerEvent("showOverlay");
    }, 360);
  }

}