export class LoadingIndicator {

  private static element: HTMLElement;

  private static setElementIfNotSetAlready() {
    if (!this.element) {
      this.element = document.querySelector(".page-loading-indicator");
    }
  }

  public static show() {
    this.setElementIfNotSetAlready();
    this.element.classList.add("visible");
  }

  public static hide() {
    this.setElementIfNotSetAlready();
    this.element.classList.remove("visible");
  }

}