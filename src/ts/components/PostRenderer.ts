export class PostRenderer {

  public static renderError(error: any) {
    let postOverlay = document.querySelector(".post-overlay");
    postOverlay.insertAdjacentHTML("beforeend", `
    <div class="load-error" data-cleanable>
      <div class="whoops">Eeeehm...</div>
      <p>Something went wrong 😕</p>
    </div>`);
  }

}