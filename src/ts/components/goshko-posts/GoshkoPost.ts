import { DataFetcher } from "../../common/DataFetcher";
import { RuntimeTemplate } from "../../common/RuntimeTemplate";
import { LoadingIndicator } from "../LoadingIndicator";

export class GoshkoPost extends HTMLElement {

  private template: string = "post_detailed";
  private data: any = {};

  constructor(data: any = {}) {
    super();
    this.setupEvents();
  }

  setupEvents() {
    document.addEventListener("openOverlay", this.onOpenOverlayHandler.bind(this));
  }

  onOpenOverlayHandler() {
    this.hide();
  }

  hide() {
    this.classList.add("hidden");
  }

  show() {
    this.classList.remove("hidden");
  }

  async loadAndRenderPost(data: any = {}) {
    this.data = data;
    let detailedData = await DataFetcher.fetchPostBySlug({
      slug: this.data.slug
    });
    this.data = Object.assign({}, this.data, detailedData.data[0]);
    LoadingIndicator.hide();
    this.render();
    this.show();
  }

  render(): void {
    this.innerHTML = RuntimeTemplate.compile(this.template, this.data);
  }

}

customElements.define("goshko-post", GoshkoPost);