import { DataFetcher } from "../../common/DataFetcher";
import { PostTeaser } from "./PostTeaser";

export class GoshkoPosts extends HTMLElement {

  public postsListElement: HTMLElement;
  public loadMoreElement: HTMLElement;

  constructor() {
    super();
    this.postsListElement = this.querySelector(".posts-list");
    this.loadMoreElement = this.querySelector("button.load-more-btn");
    this.setupEvents();
    this.loadAndRenderPosts();
  }

  setupEvents() {
    this.loadMoreElement.addEventListener("click", this.onLoadMoreButtonClick.bind(this));
  }

  onLoadMoreButtonClick() {
    this.classList.remove("idle");
    this.classList.add("loading");
  }

  async loadAndRenderPosts() {
    this.classList.remove("idle");
    this.classList.add("loading");
    let posts = await DataFetcher.fetchPosts({
      view: "teaser"
    });
    this.renderPosts(posts);
  }

  renderPosts(posts: any): void {
    let delay = 0;
     posts["data"].forEach((postData: any, index: number) => {
      let isLastIndex: boolean = (index === posts["data"].length - 1);
      delay += 50;
      setTimeout(() => {
        let postElement: PostTeaser = new PostTeaser(postData);
        this.postsListElement.appendChild(postElement);
      }, delay)

      if (isLastIndex) {
        setTimeout( () => {
          this.classList.remove("loading");
          this.classList.add("idle");
        }, delay + 50);
      }
    });
  }

}

customElements.define("goshko-posts", GoshkoPosts);