import { RuntimeTemplate } from "../../common/RuntimeTemplate";
import { EventDispatcher } from "../../common/EventDispatcher";

export class PostTeaser extends HTMLElement {

  private template: string = "post_teaser";

  private data: any = {};

  constructor(data: any) {
    super();
    this.data = data;
    this.insertAdjacentHTML("beforeend", RuntimeTemplate.compile(this.template, this.data));
    this.setupEvents();
  }

  setupEvents() {
    this.addEventListener("click", this.onClickHandler);
  }

  onClickHandler(event: MouseEvent) {
    EventDispatcher.triggerEvent("openOverlay");
    EventDispatcher.triggerEvent("growBubble", {
      x: event.clientX,
      y: event.clientY
    });

    let postOverlay: HTMLElement = document.querySelector(".post-overlay");
    postOverlay.setAttribute("data-post-slug", this.data.slug);

    // TODO: use history API to create permalinks 
    // history.replaceState(null, null, `/post/${this.data.slug}`);
  }

}

customElements.define("post-teaser", PostTeaser);