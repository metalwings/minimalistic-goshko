import { LoadingIndicator } from "../LoadingIndicator";
import { EventDispatcher } from "../../common/EventDispatcher";
import { GoshkoPost } from "../goshko-posts/GoshkoPost";

export class Overlay {
  
  private element: HTMLElement;

  constructor() {
    this.element = document.querySelector(".post-overlay");
    this.setEvents();
  }

  setEvents() {
    document.addEventListener("showOverlay", this.onShowOverlay.bind(this));  
    document.addEventListener("closeOverlay", this.onCloseOverlay.bind(this));  
  }
  
  onShowOverlay() {
    this.element.classList.add("visible");
    document.body.classList.add("post-overlay--opened");
    LoadingIndicator.show();
    this.displayPost();  
  }

  async onCloseOverlay() {
    this.element.classList.remove("visible");
    document.body.classList.remove("post-overlay--opened");
    LoadingIndicator.hide();
    await this.triggerShrinkBubble();
  }

  async triggerShrinkBubble(): Promise<any> {
    return new Promise( (resolve, reject) => {
      setTimeout(() => {
        window.scrollTo(0, 0);
        EventDispatcher.triggerEvent("shrinkBubble");
        resolve();
      }, 400);  
    });
  }

  displayPost() {
    let post: GoshkoPost = document.getElementById("detailed-view") as GoshkoPost;
    post.loadAndRenderPost({
      slug: this.element.getAttribute("data-post-slug")
    });
  }

  renderError() {

  }
  
}