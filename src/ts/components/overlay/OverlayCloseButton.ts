import { EventDispatcher } from "../../common/EventDispatcher";

export class OverlayCloseButton {  
  private element: HTMLElement;

  constructor() {
    this.element = document.querySelector(".close-button");
    this.setEvents();
  }

  setEvents() {
    this.element.addEventListener("click", this.onClickHandler.bind(this));
  }

  onClickHandler() {
    EventDispatcher.triggerEvent("closeOverlay");
    history.replaceState(null, null, '/');
  }

}