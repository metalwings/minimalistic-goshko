export class StringUtils {

  static slugify(inputURL: String) {
    inputURL = this.unicodeNormalize(inputURL);
    inputURL = this.removeNonWord(inputURL);
    inputURL = inputURL.trim()
      .replace(/ +/g, "-")
      .replace(/-+/g, "-")
      .toLowerCase();

    return inputURL;
  }

  private static removeNonWord(str: String): String {
    return str.replace(/[^0-9a-zA-Z\xC0-\xFF \-]/g, '');
  }

  private static unicodeNormalize(str: String): String {
    return str.normalize('NFD').replace(/[\u0300-\u036f]/g, "")
  }

}