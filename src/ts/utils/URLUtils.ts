export class URLUtils {
  
  public static jsonAsGETParameters(data: any = {}): string {
    return Object.keys(data).map((key) => {
      return `${encodeURIComponent(key)}=${encodeURIComponent(data[key])}`;
    }).join("&");
  }

}