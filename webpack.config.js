// var debug = process.env.NODE_ENV !== "production";
var path = require("path");

module.exports = {
  mode: 'development',
  context: __dirname,
  // devtool: debug ? "inline-sourcemap" : null,
  entry: {
    main: ["./src/ts/Main.ts",
    "./src/ts/components/goshko-posts/GoshkoPosts.ts",
    "./src/ts/components/goshko-posts/GoshkoPost.ts"]
  },
  output: {
    path: path.resolve(__dirname + "/dist/resources/js/"),
    filename: "[name].bundle.js"
  },
  module: {
    rules: [{
      test: /\.ts?$/,
      use: 'awesome-typescript-loader',
      exclude: '/node_modules/'
    }]
  },
  resolve: {
    extensions: [".ts"]
  }
};
